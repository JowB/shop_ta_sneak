import {Component, OnInit} from '@angular/core';
import {SneakersService} from '../sneakers.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-list-sneakers',
  templateUrl: './list-sneakers.component.html',
  styleUrls: ['./list-sneakers.component.css']
})
export class ListSneakersComponent implements OnInit {

  private sneakers: any;
  private id: number;

  constructor(private route: ActivatedRoute, private sneakersService: SneakersService) {
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.id = Number(params['id']);
        this.sneakers = [];
        this.sneakersService.sneakersSubject.subscribe(
          (data) => {
            this.sneakers = data;
          }
        );
        this.sneakersService.getSneakersByCategory(this.id);
      }
    );
  }

}
