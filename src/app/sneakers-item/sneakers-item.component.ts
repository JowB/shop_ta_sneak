import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SneakersService} from '../sneakers.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-sneakers-item',
  templateUrl: './sneakers-item.component.html',
  styleUrls: ['./sneakers-item.component.css']
})
export class SneakersItemComponent implements OnInit {

  private sneaker: any;
  private id: number;
  pointureControl = new FormControl('', [Validators.required]);

  constructor(private route: ActivatedRoute, private sneakerService: SneakersService) {
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.id = Number(params['id']);
        this.sneakerService.sneakerSubject.subscribe(
          (data) => {
            this.sneaker = data;
            console.log(this.sneaker);
          }
        );
        this.sneakerService.getSneakersById(this.id);
      }
    );
  }

}
