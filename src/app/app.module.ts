import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCardModule,
  MatCheckboxModule,
  MatIconModule,
  MatInputModule, MatSelectModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule
} from '@angular/material';
import { MenuComponent } from './menu/menu.component';
import { ListSneakersComponent } from './list-sneakers/list-sneakers.component';
import {RouterModule} from '@angular/router';
import {appRoutes} from './route';
import {HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import {SneakersService} from './sneakers.service';
import { SneakersItemComponent } from './sneakers-item/sneakers-item.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ListSneakersComponent,
    HomeComponent,
    SneakersItemComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatTableModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatSelectModule
  ],
  providers: [
    SneakersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
