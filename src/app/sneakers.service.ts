import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class SneakersService {

  private sneakers: any = [];
  private sneaker: {};
  sneakersSubject: Subject<any> = new Subject<any>();
  sneakerSubject: Subject<any> = new BehaviorSubject(0);
  private sneakersCategory_url = 'http://192.168.1.55/api-sts/public/main/bycategory/';
  private sneakersById_url = 'http://192.168.1.55/api-sts/public/main/sneakerbyid/';
  constructor(private http: HttpClient) { }

  emitSneakersByCategory() {
    this.sneakersSubject.next(this.sneakers);
  }

  getSneakersByCategory($id) {
    this.http.get<any>(this.sneakersCategory_url + $id).subscribe(
      (data) => {
        console.log(data);
        this.sneakers = data;
        this.emitSneakersByCategory();
      },
      (error) => console.log(error)
    );
  }

  emitSneakersById() {
    this.sneakerSubject.next(this.sneaker);
  }

  getSneakersById($id) {
    this.http.get<any>(this.sneakersById_url + $id).subscribe(
      (data) => {
        this.sneaker = data;
        this.emitSneakersById();
      },
      (error) => console.log(error)
    );
  }
}
