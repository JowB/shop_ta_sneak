export class Sneaker {
  id: number;
  category: string;
  marque: string;
  name: string;
  price: number;
  sizes: any = [];
  images: any = [];
}
