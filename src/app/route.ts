import {Routes} from '@angular/router';
import {ListSneakersComponent} from './list-sneakers/list-sneakers.component';
import {HomeComponent} from './home/home.component';
import {SneakersItemComponent} from './sneakers-item/sneakers-item.component';

export const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'category/:id', component: ListSneakersComponent},
  { path: 'sneakers/:id', component: SneakersItemComponent}
];
